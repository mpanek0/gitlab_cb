from common.methods import set_progress
from infrastructure.models import Server, CustomField
from orders.models import CustomFieldValue

#Note: In Azure or other Providers where you have a public and private_ip on a VM, its typically best to try and connect to the private IP from another server for something like MySQL, Postgres, etc

#host_name_match_string is a partial string to find in the hostname of the server are looking for

HOSTNAME_MATCH_STRING = "{{ host_name_match_string }}"
name = 'private_ip_address_{}'.format(HOSTNAME_MATCH_STRING.lower())
label = 'Private IP Address ({})'.format(HOSTNAME_MATCH_STRING.upper())

def run(job, resource, **kwargs):
  #find a server in the server set that has the hostname string you supplied somewhere in the name
  for srv in job.server_set.all():
    set_progress("testing server in server set for hostname match: {}".format(srv.hostname))
    if (srv.hostname.find("{{ host_name_match_string }}")) > 1:
      set_progress("Found server in server set for hostname match!")
      server = srv
      break
    #endif
  #next
  if server:
    set_progress("Setting {} -- {} -- for ip address on server: {}".format(name,label,server))
    
    cf, _ = CustomField.objects.get_or_create(name=name, label=label, type='STR')

    resource.update_cf_value(cf, server.nics.all()[0].private_ip, job.owner)
    return '', '', ''
  else:
    return 'FAILURE','FAILURE: {{ host_name_match_string }} server not found for Private IP lookup',''